# Creating Release Candidates

Release Candidates (RCs) are pre-release versions of the next major version of
GitLab CE and EE. The first RC, appropriately called RC1, is typically created
as early as possible during the first week of each month. This allows us to
start QA before the feature freeze. The first RC after the 7th will be the
previous one, with a merge from `master`.

Every release will have several RCs, and there is no limit on the final number.
Usually, at least four RCs are made before the official release. This ensures
new changes are tried in production, at scale, and new bugs can be fixed before
the official release.

Usually RC1 has the most number of migrations that should be deployed into
production as soon as possible. Release managers should prioritize getting RC1
deployed. If there are showstopping issues with RC1 (e.g. failed migrations,
critical bugs that prevent logins, etc.), a new RC2 should be created
that contains the **minimal set of changes** needed to deploy the
package. Avoid picking more patches that may slow down the release, especially
those that contain new migrations.

## About the "Release Candidate" naming

We call them "Release Candidates" even though the early RCs are closer to Beta
than real RC. This simplifies our releasing/packaging tools and scripts. This
approach is coherent with packages.gitlab.com since our RC packages are
available under [`gitlab/unstable`].

## Guides

- [Creating RC1](#creating-rc1)
- [Feature freeze RC](#feature-freeze-rc)
- [Creating subsequent RCs](#creating-subsequent-rcs)

### Creating RC1

#### Step 1: Update the ["Installation from Source" guide]

> **Note:** This only needs to be done for the GitLab CE repository. Changes
will be merged into GitLab EE.

1. Update the name of the `stable` branch in **Clone the Source**.
   There are two occurrences.
1. Depending on changes in the upcoming release, you may need to add or remove
   sections. For example, in GitLab 8.0 we had to add the section about
   installing `gitlab-workhorse` (called `gitlab-git-http-server` at the time).

["Installation from Source" guide]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/install/installation.md

#### Step 2: Create the "Update" guides

Each major release of GitLab needs a corresponding [update guide](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/update)
with instructions on how to manually upgrade from the previous major release.

> **Note:** GitLab CE and EE each have specific guides that need to be created.
Make sure to do both!

> **Note:** For the examples below, we're going to be using GitLab 8.2 as an
example of the upcoming release, and 8.1 as an example of the previous release.

##### GitLab CE

1. Copy the previous update guide to use as a template:

    ```sh
    # NOTE: This command is an example! Update it to reflect new version numbers.
    cp doc/update/8.0-to-8.1.md doc/update/8.1-to-8.2.md
    ```

1. Update the versions in the top-level header.
1. Update the name of the latest `X-Y-stable[-ee]` branch **Get latest code**.
   There are two occurrences.
1. Update the names of the `X-Y-stable` branches in **Update configuration
   files**.
1. Update references to the "previous version" in **Things went south?** and the
   link to the previous guide.
1. Add any special instructions specific to this version. For example, maybe
   this version adds a new external dependency not in the previous version.
1. Read through the entire guide to make sure it makes sense. For example, maybe
   the previous version required special steps that no longer apply this
   version.

##### GitLab EE

GitLab EE releases include guides to migrate from the CE version of a major
release to the EE version of the same release.

1. Copy the previous update guide to use as a template:

    ```sh
    # NOTE: This command is an example! Update it to reflect new version numbers.
    cp doc/update/8.1-ce-to-ee.md doc/update/8.2-ce-to-ee.md
    ```

1. Update the version numbers in the top-level header and introduction. There
   are three occurrences.
1. Update the name of the `stable` branch in **Get the EE code**.
1. Update the version number in **Things went south?** and the name of the
   `stable` branch in **Revert the code to the previous version**.

#### Step 3: Tag the RC1 version

Use the [`release`](rake-tasks.md#releaseversion) Rake task:

```sh
# NOTE: This command is an example! Update it to reflect new version numbers.
bundle exec rake "release[8.2.0-rc1]"
```

### Feature freeze RC

This is the first RC after the code freeze.

#### Step 1: Pick a "freeze" commit on gitlab-ce master branch

(There is a plan to automate this step: https://gitlab.com/gitlab-org/release-tools/issues/245)

Select a commit on [gitlab-ce master
branch](https://gitlab.com/gitlab-org/gitlab-ce/commits/master) which will be
used as the "cut" for this release (everything up to this commit in `master`
branch will be included in the release). If this RC is being done next morning
after the feature freeze you can use just `master` for merging new code into
preparation branches.

> **Note:** If this RC is being done with some delay you may want to use
an older commit (closer to the freeze time). Copy the commit's SHA (which you
want to use as the "cut") and pick the freeze commit on EE master branch (it's
possible the master CE was not synced to EE yet, so it might be an older or
EE-only commit SHA, which is not a problem - CE is synced to EE when preparing
RC preparation MRs). Then update the channel message in step 2 bellow - not
everything 'after this point' but everything 'after the freeze commit' will go
into next release. And in step 3, when merging CE and EE into preparation
branches, use these freeze commits instead of `master`.

#### Step 2: Notify all on `#development` slack channel

Replace `X` and `Y` with major and minor version being released (e.g. 11 and
4).

```
@channel

`X-Y-stable` branch has been created. Everything merged into `master`
after this point will go into next month's release. Only regression and security fixes
will be cherry-picked into `X-Y-stable`.

Please ensure that merge requests have the correct milestone (`X.Y` for this release)
and the `Pick into X.Y` label.

From now on, please follow the "After the 7th" process:
https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#after-the-7th
```

#### Step 3: Integrating changes from `master` into `X-Y-stable`

This step should be done after [preparation MRs](https://gitlab.com/gitlab-org/release/docs/blob/master/general/picking-into-merge-requests.md)
have been created (these are created in the first step of each RC release).

Once the `X-Y-stable` branch is created, it is the sole source of future
releases for that version. From the 8th, merge requests will either
be [cherry-picked] into `X-Y-stable` by the release manager, or a second merge
request targeting `X-Y-stable` (instead of `master`) should be opened.

Developers are responsible for notifying the release manager that a merge
request is ready to be moved into `X-Y-stable` by following the ["After the
7th" process].

Feature freeze RC is usually not the first RC (RC1 is usually created before
feature freeze) and `X-Y-stable` branch already exists. Now you have to merge
all commits merged into `master` branch between RC1 and `master`. You
can use following command:

```
# checkout to the prepration branch for the current RC
git checkout X-Y-stable-prepare-rcZ
git merge master

# and for EE branch
git checkout X-Y-stable-ee-prepare-rcZ
git merge master (of EE repo)
```

And also cherry-pick MRs created in step 3 (`.gitignore` and `Dockerfile` changes) and step 4 (licenses).

Do not forget to sync `master` and `X-Y-stable` branch also for `omnibus-gitlab`.

#### Step 4: Update the `.gitignore`, and `Dockerfile` templates on `gitlab-ce`

Run `bin/rake gitlab:update_templates` and open a merge request to `master`

This will update the [`.gitignore`](https://github.com/github/gitignore),
and [`Dockerfile`](https://gitlab.com/gitlab-org/Dockerfile) templates.

Ensure the MR is merged and marked Pick into X.Y.

#### Step 5: Update the dependencies license list on `gitlab-ce`

Run `bin/bundle exec license_finder report --format=csv --save=vendor/licenses.csv` and
open a merge request to `master`.

Ensure the MR is merged and marked Pick into X.Y.

Cherry-pick both MRs into preparation branches.

---

### Creating subsequent RCs

#### Step 1: Bring changes to the `stable` branches

[Cherry-pick][cherry-picked] CE and EE merge requests into their respective `stable` branch.
Keep in mind that after RC1 only regressions and security fixes should be
cherry-picked into `stable` branch.

#### Step 2: Merge CE `stable` into EE `stable`

Ensure that CE's `X-Y-stable` branch is merged into EE's `X-Y-stable-ee`. See
the [Merge GitLab CE into EE](merge-ce-into-ee.md#merging-a-ce-stable-branch-into-its-ee-counterpart)
guide.

#### Step 3: Tag the RC version

Use the [`release`](rake-tasks.md#releaseversion) Rake task:

```sh
# NOTE: This command is an example! Update it to reflect new version numbers.
bundle exec rake "release[8.2.0-rc2]"
```

[`gitlab/unstable`]: https://packages.gitlab.com/gitlab/unstable
["After the 7th" process]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#after-the-7th
[cherry-picked]: pick-changes-into-stable.md

---

[Return to Guides](../README.md#guides)
