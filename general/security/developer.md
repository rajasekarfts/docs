# Security Releases (Critical / Non-critical) as a Developer

The release deadlines for a critical or non-critical security release are different.
Check the [Release deadlines](process.md) first to know when the security
fixes have to be merged by.

As a developer working on a fix for a security vulnerability, your main concern
is not disclosing the vulnerability or the fix before we're ready to publicly
disclose it.

To that end, you'll need to be sure that all of the development for a fix
happens on [dev.gitlab.org], because it's not publicly-accessible.

[dev.gitlab.org]: https://dev.gitlab.org/

## Process

As with most GitLab development, a security fix starts with an issue identifying
the vulnerability. In this case, it should be a confidential issue on
[gitlab.com].

Once a security issue is assigned to a developer, we follow the same merge
request and code review process as any other change, but on [dev.gitlab.org].
All security fixes are released for [at least three monthly releases], and you
will be responsible for creating backports as well.

[gitlab.com]: https://gitlab.com/

### Preparation

- Before starting, run `scripts/security-harness` in the CE/EE repo you will implement
  the fix in. This script will install a Git `pre-push` hook that will prevent
  pushing to any remote besides `dev.gitlab.org`, in order to prevent accidental
  disclosure.
- Create a new [issue on org](https://dev.gitlab.org/gitlab/gitlabhq/issues/new) using the [Security Developer Workflow] template.
- Security vulnerabilities that exist in **both** CE and EE should be fixed in
  the [CE project on org](https://dev.gitlab.org/gitlab/gitlabhq).
- Security vulnerabilities that exist only in EE should be fixed in the [EE
  project on org](https://dev.gitlab.org/gitlab/gitlab-ee).
- Security vulnerabilities that exist in Omnibus should be fixed in the [Omnibus
  project on org](https://dev.gitlab.org/gitlab/omnibus-gitlab).

[security developer workflow]: https://dev.gitlab.org/gitlab/gitlabhq/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md

### Branches

Because all security fixes go into [at least three monthly releases], you'll be
creating at least three branches for your fix.

Security fixes are merged into a `security-X-Y` branch rather than directly into
the `X-Y-stable` branches, in order to avoid releasing a fix unexpectedly. For
example, a security fix for the `10.6` release should be based on, and
targeting, `security-10-6`.

> **Note:** If a `security-X-Y` branch doesn't yet exist, you can create one by
> branching it directly off of the corresponding `X-Y-stable` branch and pushing
> to org.

Your branch name must start with `security`, such as: `security-rs-milestone-xss-10-6`.

### Development

Generally you'll want to add a test case that verifies the vulnerability, then
develop a fix that satisfies the test.

Once a fix is in place, you **must** add a clear, concise [changelog
entry](https://docs.gitlab.com/ee/development/changelog.html):

- A changelog entry is required for security fixes. No exceptions.
- It **must not** have a `merge_request` value. The merge request will only
  exist on org, and its ID would point to the wrong merge request on .com.
- `type` **must** be`security`.

### Create merge requests

Open a merge request in the relevant [dev.gitlab.org] project. Target the
`security-X-Y` branch that your branch was based off of.

A security merge request:

- **must** link to the developer security developer workflow issue on `dev.gitlab.org`
- **must** use the exact same title across all ports, with a
  clear identifier for the version it applies to. For example:
  - `[10.6] Prevent stored XSS in code blocks`
  - `[10.5] Prevent stored XSS in code blocks`
  - `[10.4] Prevent stored XSS in code blocks`

Once your merge requests are created, you should update the security developer workflow issue
to include links to all of them.

#### `secpick` script

This is a small script that helps cherry-picking across multiple releases. It will stop
if there is a conflict while cherry-picking, otherwise will push the change to `org`.

The list of options available running:

```
$ bin/secpick --help
```

For example:

```
bin/secpick -v 10.6 -b security-fix-mr-issue -s SHA
```

It will change local branches to push to a new security branch for each specified release,
meaning that local changes should be saved prior to running the script.

This is only useful if we have squashed the original MR commits into a single one, easier
to cherry-pick.

### Final steps

After the Release Manager promotes the packages to public, your fix will need to go into into the `master`
branch to ensure that it's included in all future releases. The release manager will merge
the MR, so make sure it's assigned to them once it's reviewed and ready.

Be sure to run `scripts/security-harness` again to enable pushing to remotes
other than `dev.gitlab.org`!

---

[Return to Security Guide](process.md)

[at least three monthly releases]: https://docs.gitlab.com/ee/policy/maintenance.html#security-releases
